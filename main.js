let navbarOpenIcon = document.getElementById('navbarOpenIcon');
let navbarCloseIcon = document.getElementById('navbarCloseIcon');
let navbarContainer = document.getElementById('navbarContainer');
let navbarDropdown = document.getElementById('navbarDropdown');
let introSectionImage = document.getElementById('introSectionImage');

navbarContainer.addEventListener('click',(click_event)=>{
    navbarCloseIcon.classList.toggle('hide-element');
    navbarOpenIcon.classList.toggle('hide-element');
    introSectionImage.classList.toggle('hide-element');
    if(navbarDropdown.style.display === 'none' || navbarDropdown.style.display === '' ){
        navbarDropdown.style.display = 'flex';
    }
    else{
        navbarDropdown.style.display = 'none';
    }
    
})